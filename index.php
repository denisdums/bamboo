<?php

include ('App/AppRequirements.php');
ob_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Define your meta infos -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Define the title of your app -->
    <title>Frappé | <?php

        if (isset($_GET['action'])){
            echo $_GET['action'];
        }
        else{
           echo "Accueil"   ;
        }

        ?></title>

    <!-- Put here your links -->
    <link href="App/assets/css/style.css" rel="stylesheet">

</head>

<body data-barba="wrapper">



    <?php
    include('App/views/templates/header.php');
    include('App/views/loader.php');

    ?>

            <?php
            #Redirections_Maker
                if (isset($_GET['action'])){
                    $action = new Action($_GET['action']);
                    $action->getAction();
                }
                else{
                    include ('App/views/home.php');
                }
                ob_end_flush();
            ?>

    <?php include('App/views/templates/footer.php'); ?>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="App/assets/js/main.js"></script>

</body>
</html>
